! SUBMODULE (logger) ErrorWarn
! IMPLICIT NONE

! CONTAINS
   subroutine error_stop_n(n, cat, logg)
      !=========================================================================
      !
      !=========================================================================
      IMPLICIT NONE

      integer,  intent(in)                       :: n
      integer,  intent(in), optional             :: cat
      character(len=11)                          :: source
      class (logging), intent(in), optional    :: logg
      !-------------------------------------------------------------------------

      source='[Unknown]  '

      if ( present(cat) ) then
         select case (cat)
         case(0)
            source='[EMERGENCY]'
         case(1)
            source='[ALERT]'//char(9)
         case(2)
            source='[CRITICAL]'//char(9)
         case(3)
            source='[ERROR]'//char(9)
         case(4)
            source='[WARNing]'//char(9)
         case default
            source='[Unknown]'//char(9)
         endselect
      endif


      select case(n)
      case (19)
         write(stderr_unit,*) source//'Error_code (19): Error in the arguments parsed.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Error_code (19): Error in the arguments parsed.'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 19
      case (20)
         write(stderr_unit,*) source//'Error_code (20): Errors for trial.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Error_code (20): Errors for trial.'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 20
      case (21)
         write(stderr_unit,*) source//'Error_code (21): Errors when reading the input file.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Error_code (21): Errors when reading the input file.'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 21
      case (22)
         write(stderr_unit,*) source//'Error_code (22): Error when reading the input image with the fortran subroutine &
         & read_Fort_image from the image_mod module.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Error_code (22): Error when reading the input image with the fortran &
         & subroutine read_Fort_image from the image_mod module.'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 22
      case (32)
         write(stderr_unit,*) source//'Error_code (32): The function initialise_outputs received an invalid argument.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Error_code (32): The function initialise_outputs received an invalid argument.'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 32
      ! case (666)
      !    write(stderr_unit,*) source//'Unknown Error_code (666): Try to debug by yourself. Sorry!'
      !    if (present(logg)) then
      !       write(logg%file_unit,*) source//'Error_code (20): Errors for trial.'
      !       close(logg%file_unit)
      !    endif
      !    ERROR STOP 666 is not available in PGI compiler
      !    STOP 666
      case default
         write(stderr_unit,*) source//'Unknown Error_code ('//stringify(n)//'): Try to debug by yourself. Sorry!'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Unknown Error_code ('//stringify(n)//'): Try to debug by yourself. Sorry!'
            close(logg%file_unit)
         endif
         ! ERROR STOP is not available in PGI compiler
         STOP 666
      endselect
   end subroutine error_stop_n


   ! module
   subroutine warn_n(n, logg)
      IMPLICIT NONE
      integer,  intent(in)                       :: n
      class (logging), intent(in), optional    :: logg
      character(len=11)                          :: source

      source='[WARNing]  '

      select case(n)
      case (20)
         write(stdout_unit,*) source//'Warn_code (20): Errors for trial.'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Warn_code (20): Errors for trial.'
         endif
      ! case (666)
      !    write(stdout_unit,*) source//'Unknown Warn_code (666): Try to debug by yourself. Sorry!'
      !    if (present(logg)) then
      !       write(logg%file_unit,*) source//'Warn_code (20): Errors for trial.'
      !       close(logg%file_unit)
      !    endif
      !    ERROR STOP 666 is not available in PGI compiler
      !    STOP 666
      case default
         write(stdout_unit,*) source//'Unknown Warn_code ('//stringify(n)//'): Try to debug by yourself. Sorry!'
         if (present(logg)) then
            write(logg%file_unit,*) source//'Unknown Warn_code ('//stringify(n)//'): Try to debug by yourself. Sorry!'
            close(logg%file_unit)
         endif
         ! ERROR STOP 666 is not available in PGI compiler
         ! STOP 666
      endselect
   end subroutine warn_n

! END SUBMODULE ErrorWarn
