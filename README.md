# fLogger

A single file libary for Fortran log Libary.


## Requirements

* [PorPre](https://gitlab.com/RomainNoel/porpre) a portable precision library
* [FoST](https://gitlab.com/RomainNoel/fost) a string manipulation libary
